

/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
// End Js  select box

// hieu ung chay so 
function inVisible(element) {
    //Checking if the element is
    //visible in the viewport
    var WindowTop = $(window).scrollTop();
    var WindowBottom = WindowTop + $(window).height();
    var ElementTop = element.offset().top;
    var ElementBottom = ElementTop + element.height();
    //animating the element if it is
    //visible in the viewport
    if ((ElementBottom <= WindowBottom) && ElementTop >= WindowTop)
      animate(element);
  }
  
  function animate(element) {
    //Animating the element if not animated before
    if (!element.hasClass('ms-animated')) {
      var maxval = element.data('max');
      var html = element.html();
      element.addClass("ms-animated");
      $({
        countNum: element.html()
      }).animate({
        countNum: maxval
      }, {
        //duration 5 seconds
        duration: 2000,
        easing: 'linear',
        step: function () {
          element.html(Math.floor(this.countNum) + html);
        },
        complete: function () {
          element.html(this.countNum + html);
        }
      });
    }
  
  }
  
  //When the document is ready
  $(function () {
    //This is triggered when the
    //user scrolls the page
    $(window).scroll(function () {
      //Checking if each items to animate are 
      //visible in the viewport
      $("h2[data-max]").each(function () {
        inVisible($(this));
      });
    })
  });
  
  

$(document).ready(function($) {
    $('.slider-head').slick({
    
    draggable: true,
    autoplay: true,
    autoplaySpeed: 1800,
    infinite: true,
    cssEase: 'ease-in-out',
    dots: false,
    arrows: false,
    infinite: true,
    fade:true,
    });
    });
    
  
$(document).ready(function () {
    $('.slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
    
     });

     

    let displayItem = 7;
    let currentPositon = displayItem *100;
    let numOfSdilerImages = $('#slider-container .slide').length;
    let maxPositon = numOfSdilerImages * 100;


    console.log()
    if (maxPositon > currentPositon) {
        let isBack = false;
        setInterval(() => {
            if (isBack) {
                prev();
                currentPositon  -=100;

                if (currentPositon <= (displayItem * 100)) isBack = false;
            }

            if (currentPositon < maxPositon & !isBack) {
                next();
                currentPositon += 100;
               
            }

            if(currentPositon == maxPositon) isBack = true;

        },
            100);
    };

    //call the function when ready
    slideShow();


    //Actually define the slideShow()
    function slideShow() {

        //*** Conditional & Variables ***//

        //Define the current img
        var current = $('#slider .show');
        //If index != 0/false then show next img
        var next = current.next().length ?
            current.next() :
            // if index == false then show first img
            current.siblings().first();

        //*** Swap out the imgs and class ***//
        current.hide().removeClass('show');
        next.fadeIn("slow").addClass('show');


        //*** Repeat function every 3 seconds ***//
        setTimeout(slideShow, 3000);

    };

}); 
// Js Menu mobile header



// Elements slider-explore-our-projects
$(document).ready(function () {
    $('.slider-explore-our-projects').owlCarousel({
      loop: true,
      rewind: true,
      margin: 10,
      // autoplayHoverPause: false,
      // autoplay: 700,
      // smartSpeed: 700,
      responsiveClass: true,
      responsive: {
        0: {
          items: 2,
          nav: true
        },
        600: {
          items: 3,
          nav: false
        },
        1000: {
          items: 3,
          nav: true,
          loop: false,
          margin: 0
        },
        1300: {
          items: 3,
          nav: true,
          loop: false,
          margin: 0
        }
      }
    })
  });

// Sticky menu tab  to srcoll page lisst service 3

$(document).ready(function () {
    $(".menu-tab-service-3 ul li a").click(function () {
        $(".menu-tab-service-3 ul li a").removeClass("show-active");
        $(this).addClass("show-active");
    });
});


// $(".loading-bar").slick({
//     centerMode: true,
//     // centerPadding: "80px",
//     dots: false,
//     infinite: false,
//     speed: 300,
//     slidesToShow: 6,
//     slidesToScroll: 6,
//     focusOnSelect: true,
//     asNavFor: ".labels"
//   });
  
//   $(".labels").slick({
//     slidesToShow: 6,
//     slidesToScroll: 6,
//     arrows: false,
//     fade: true,
//     slidesToShow: 6,
//     draggable: false,
//     asNavFor: ".loading-bar"
//   });


  