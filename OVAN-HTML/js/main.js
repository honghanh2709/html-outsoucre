
// Menu mobile
$(".menu-mobile i.ti-menu").click(function () {
  $('header').addClass('show-menu');
});
$(".menu-mobile i.ti-close").click(function () {
  $('header').removeClass('show-menu');
});

$("nav > ul > li").on("click", function () {
  $("nav > ul > li").removeClass("active");
  $(this).addClass("active");
});

//Search mobile
$(".btn-search-mb").click(function () {
  $('.top-head').addClass('show-search-mb');

});
$(".search-head .close-search").click(function () {
  $('.top-head').removeClass('show-search-mb');

});
/* ---------Js input file ----------*/
// (function() {

//   'use strict';

//   $('.input-file').each(function() {
//     var $input = $(this),
//         $label = $input.next('.js-labelFile'),
//         labelVal = $label.html();

//    $input.on('change', function(element) {
//       var fileName = '';
//       if (element.target.value) fileName = element.target.value.split('\\').pop();
//       fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
//    });
//   });

// })();


// Sticky head menu to srcoll
$(window).scroll(function () {
  let contentHeadHeigh = $(".content-head").height();
  var sticky = $('header'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});
// Sticky box cart  to srcoll
$(window).scroll(function () {
  let contentHeadHeigh = $(".section-feeback-detail").height();
  var sticky = $('.sticky-cart-product'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('show-sticky-cart-product');
  if (scroll == 0)
    sticky.removeClass('show-sticky-cart-product');
});


// Elements Animation
if ($('.wow').length) {
  var wow = new WOW(
    {
      boxClass: 'wow',      // animated element css class (default is wow)
      animateClass: 'animated', // animation css class (default is animated)
      offset: 0,          // distance to the element when triggering the animation (default is 0)
      mobile: false,       // trigger animations on mobile devices (default is true)
      live: true       // act on asynchronously loaded content (default is true)
    }
  );
  wow.init();
}


// Elements Slider homepage
$(document).ready(function () {
  $('.head-slider-home').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
})

// Elements section Slider cac dong san pham
$(document).ready(function () {
  $('.slider-nhan-hang').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 2,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 6,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
})
// Elements section Slider section-danh-muc-san-pham-home
$(document).ready(function () {
  $('.list-danhmuc').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 2,
        nav: true
      },
      600: {
        items: 3,
        nav: false
      },
      1000: {
        items: 4,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 5,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
})

/* ----------Js select box -----------------*/

/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

/* ----------ENd Js select box -----------------*/


/* ---------- Js Slider product detail -----------------*/

$(document).ready(function () {
  $("#content-slider").lightSlider({
    loop: true,
    keyPress: true
  });
  $('#image-gallery').lightSlider({
    gallery: true,
    item: 1,
    thumbItem: 9,
    slideMargin: 0,
    // speed:500,
    // auto:true,
    // loop:true,
    // autoplay:false,
    onSliderLoad: function () {
      $('#image-gallery').removeClass('cS-hidden');
    }
  });
});
/* ---------- Js show full gallery -----------------*/
$(document).ready(function () {
  $(".show-gallery").click(function () {
    $(".popup-gallery").addClass("show");
  });
  $(".close-popup").click(function () {
    $(".popup-gallery").removeClass("show");
    let vid = $('#id-gallery');

    vid.trigger('pause')
    vid.get(0).currentTime = 0;
  });
});

// Elements Slider feedback tu chueyn gia
$(document).ready(function () {
  $('.slider-feeback-detail').owlCarousel({
    loop: true,
    margin: 10,
    // autoplayHoverPause:false,
    //  autoplay: 6000,
    // smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
})

// Js expand nut " xem them "
$(".expand-content-editor button").click(function () {
  $(".description-body-affilate").toggleClass("heightAuto");
});



// Update js slider page promotion
$(document).ready(function () {
  $('.slider-promotion').owlCarousel({
    loop: true,
    margin: 10,
    // autoplayHoverPause: false,
    // autoplay: 6000,
    // smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 2,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 4,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 6,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
})
/* Update Filter promotion mobile */
$(".btn-filter").click(function () {
  $('.col-filter').addClass('show-filter');
});
$(".col-filter .close-filter").click(function () {
  $('.col-filter').removeClass('show-filter');

});

// Js expand nut " xem them danh gia"
$(".expand-content-editor-rating button").click(function () {
  $(".content-rating").toggleClass("heightAuto");
});

// Elements Slider show full images page detail
$(document).ready(function () {
  $('.slider-show-full-img').owlCarousel({
    loop: true,
    margin: 10,
    // autoplayHoverPause: false,
    // autoplay: 6000,
    // smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});